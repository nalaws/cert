# ChainMaker证书延长工具

本工具基于长安链v2.3.0源码进行开发，可以对v2.3.0集群的证书进行延长操作

### 使用步骤

1. 下载源码
```shell
mkdir -p $GOPATH/src/chainmaker.org/chainmaker/tools
cd $GOPATH/src/chainmaker.org/chainmaker/tools
git clone <源码地址>
```

2. 编译源码

```shell
go build main.go
```

3. 测试

进入`example`目录,运行`delay.sh`脚本命令进行测试
```shell
cd example
delay.sh ca   # 延长CA证书
delay.sh node # 延长节点证书
delay.sh user # 延长用户证书
```
**说明：**

1、使用者可以根据示例脚本进行更改

2、延长节点证书、用户证书，延长后把新证书替换掉所有旧证书即可使用

3、更新`ca`证书需要参考官方更新`ca`证书步骤，更新`ca`证书后需要更新该组织下的所有证书

4、证书更新不会改变`nodeid`、证书地址
