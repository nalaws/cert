package command

import (
	"fmt"
	"path/filepath"
	"strings"

	"chainmaker.org/chainmaker/common/v2/crypto"
	"github.com/spf13/cobra"

	"chainmaker.org/chainmaker/tools/cert/config"
	cmcrypto "chainmaker.org/chainmaker/tools/cert/crypto"
)

func CACmd() *cobra.Command {
	caCmd := &cobra.Command{
		Use:   "ca",
		Short: "delay ca cert",
		Long:  "delay ca cert",
		RunE: func(cmd *cobra.Command, args []string) error {
			return delayCA()
		},
	}

	caCmd.Flags().StringVarP(&rootPath, "ca", "", fmt.Sprintf("%s/wx-org1.chainmaker.org/ca", OutputDir), "specify the directory for storing CA certificates, which must include ca.key and ca.crt")

	return caCmd
}

func delayCA() error {
	cryptoGenConfig := config.GetCryptoGenConfig()

	for _, item := range cryptoGenConfig.Item {
		for i := 0; i < int(item.Count); i++ {
			orgName := fmt.Sprintf("%s%d.%s", item.HostName, i+1, item.Domain)
			if item.Count == 1 {
				orgName = fmt.Sprintf("%s.%s", item.HostName, item.Domain)
			}
			keyType := crypto.AsymAlgoMap[strings.ToUpper(item.PKAlgo)]
			hashType := crypto.HashAlgoMap[strings.ToUpper(item.SKIHash)]

			caPath := rootPath
			if caPath == "" {
				caPath = filepath.Join(OutputDir, orgName, "ca")
			}

			caCN := fmt.Sprintf("ca.%s", orgName)
			caSANS := append(item.CA.Specs.SANS, caCN)
			config.SetPrivKeyContext(keyType, orgName, 0, "ca")
			if err := cmcrypto.DelayCA(caPath,
				item.CA.Location.Country, item.CA.Location.Locality, item.CA.Location.Province, "root-cert", orgName, caCN,
				item.CA.Specs.ExpireYear, caSANS, keyType, hashType); err != nil {
				return err
			}
		}
	}
	return nil
}
