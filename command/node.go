package command

import (
	"errors"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"chainmaker.org/chainmaker/common/v2/crypto"
	"github.com/spf13/cobra"

	"chainmaker.org/chainmaker/tools/cert/config"
	cmcrypto "chainmaker.org/chainmaker/tools/cert/crypto"
)

func NodeCmd() *cobra.Command {
	nodeCmd := &cobra.Command{
		Use:   "node",
		Short: "delay node cert",
		Long:  "delay node cert",
		RunE: func(cmd *cobra.Command, args []string) error {
			return delayCert()
		},
	}

	nodeCmd.Flags().StringVarP(&rootPath, "ca", "", fmt.Sprintf("%s/wx-org1.chainmaker.org/ca", OutputDir), "specify the directory for storing CA certificates, which must include ca.key and ca.crt")
	nodeCmd.Flags().StringVarP(&delayCertPath, "cert", "", fmt.Sprintf("%s/wx-org1.chainmaker.org/user/client1/client1.sign.crt", OutputDir), "delay certificate file. The directory must contain 'xxx.key' file for the delay certificate")
	nodeCmd.Flags().Int32VarP(&delayYear, "year", "y", 10, "delay year")

	return nodeCmd
}

func delayCert() error {
	caKeyPath := fmt.Sprintf("%s/ca.key", rootPath)
	if _, err := os.Stat(caKeyPath); os.IsNotExist(err) {
		log.Fatalln(err)
		return err
	}
	caCertPath := fmt.Sprintf("%s/ca.crt", rootPath)
	if _, err := os.Stat(caCertPath); os.IsNotExist(err) {
		log.Fatalln(err)
		return err
	}
	certPath := delayCertPath
	if _, err := os.Stat(certPath); os.IsNotExist(err) {
		log.Fatalln(err)
		return err
	}

	if !strings.HasSuffix(certPath, ".crt") {
		err := errors.New("cert file name error")
		log.Fatalln(err)
		return err
	}

	pos := strings.LastIndex(certPath, ".")
	keyPath := fmt.Sprintf("%s.key", certPath[:pos])
	if _, err := os.Stat(keyPath); os.IsNotExist(err) {
		log.Fatalln(err)
		return err
	}

	path := filepath.Dir(certPath)
	fileName := filepath.Base(certPath)
	raws := strings.Split(fileName, ".")
	if len(raws) != 3 {
		log.Fatal("cert name error")
		return errors.New("cert name error")
	}
	name := fmt.Sprintf("%s.%s", raws[0], raws[1])

	var (
		typeName string
		pairType cmcrypto.KeyPairType
	)
	switch true {
	case strings.HasPrefix(raws[0], "consensus"):
		typeName = "consensus"
		if raws[1] == "sign" {
			pairType = cmcrypto.KeyPairTypeNodeSign
		} else {
			pairType = cmcrypto.KeyPairTypeNodeTLS
		}
	case strings.HasPrefix(raws[0], "common"):
		typeName = "common"
		if raws[1] == "sign" {
			pairType = cmcrypto.KeyPairTypeNodeSign
		} else {
			pairType = cmcrypto.KeyPairTypeNodeTLS
		}
	case strings.HasPrefix(raws[0], "admin"):
		typeName = "admin"
		if raws[1] == "sign" {
			pairType = cmcrypto.KeyPairTypeUserSign
		} else {
			pairType = cmcrypto.KeyPairTypeUserTLS
		}
	case strings.HasPrefix(raws[0], "client"):
		typeName = "client"
		if raws[1] == "sign" {
			pairType = cmcrypto.KeyPairTypeUserSign
		} else {
			pairType = cmcrypto.KeyPairTypeUserTLS
		}
	case strings.HasPrefix(raws[0], "light"):
		typeName = "light"
		if raws[1] == "sign" {
			pairType = cmcrypto.KeyPairTypeUserSign
		} else {
			pairType = cmcrypto.KeyPairTypeUserTLS
		}
	default:
		log.Fatal("cert name error")
		return errors.New("cert name error")
	}
	j, err := strconv.Atoi(raws[0][len(typeName):])
	if err != nil {
		log.Fatal("cert name error")
		return err
	}

	if delayYear < 1 {
		err = errors.New("delay year error.")
		log.Fatal(err)
		return err
	}

	cryptoGenConfig := config.GetCryptoGenConfig()
	for _, item := range cryptoGenConfig.Item {
		for i := 0; i < int(item.Count); i++ {
			orgName := fmt.Sprintf("%s%d.%s", item.HostName, i+1, item.Domain)
			if item.Count == 1 {
				orgName = fmt.Sprintf("%s.%s", item.HostName, item.Domain)
			}
			keyType := crypto.AsymAlgoMap[strings.ToUpper(item.PKAlgo)]
			hashType := crypto.HashAlgoMap[strings.ToUpper(item.SKIHash)]

			config.SetPrivKeyContext(keyType, orgName, j, typeName)
			return cmcrypto.DelayCert(path, caKeyPath, caCertPath, name, delayYear, hashType, pairType)
		}
	}
	return nil
}
