package command

import (
	"fmt"

	"github.com/spf13/cobra"
)

func UserCmd() *cobra.Command {
	userCmd := &cobra.Command{
		Use:   "user",
		Short: "delay user cert",
		Long:  "delay user cert",
		RunE: func(cmd *cobra.Command, args []string) error {
			return delayCert()
		},
	}

	userCmd.Flags().StringVarP(&rootPath, "ca", "", fmt.Sprintf("%s/wx-org1.chainmaker.org/ca", OutputDir), "specify the directory for storing CA certificates, which must include ca.key and ca.crt")
	userCmd.Flags().StringVarP(&delayCertPath, "cert", "", fmt.Sprintf("%s/wx-org1.chainmaker.org/user/client1/client1.sign.crt", OutputDir), "delay certificate file. The directory must contain 'xxx.key' file for the delay certificate")
	userCmd.Flags().Int32VarP(&delayYear, "year", "y", 10, "delay year")

	return userCmd
}
