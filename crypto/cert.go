package cmcrypto

import (
	"crypto/x509"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"time"

	"chainmaker.org/chainmaker/common/v2/cert"
	"chainmaker.org/chainmaker/common/v2/crypto"
)

type KeyPairType uint8

const (
	KeyPairTypeUserSign   = 0
	KeyPairTypeUserTLS    = 1
	KeyPairTypeUserTLSEnc = 2 //only used for GMTLS double cert mode

	KeyPairTypeNodeSign   = 3
	KeyPairTypeNodeTLS    = 4
	KeyPairTypeNodeTLSEnc = 5 //only used for GMTLS double cert mode
)

func DelayCA(
	path, c, l, p, ou, org, cn string,
	expireYear int32,
	sans []string,
	keyType crypto.KeyType,
	hashType crypto.HashType) error {

	caKeyPath := filepath.Join(path, "ca.key")

	if _, err := os.Stat(caKeyPath); os.IsNotExist(err) {
		log.Fatalln(err)
		return err
	}

	caCertPath := filepath.Join(path, "ca.crt")
	_, err := os.Stat(caCertPath)
	if err == nil || os.IsExist(err) {
		data := time.Now()
		back := filepath.Join(path, fmt.Sprintf("ca.crt.%d", data.Unix()))
		os.Rename(caCertPath, back)
	} else {
		log.Fatalln(err)
		return err
	}

	privKey, err := parsePrivateKey(caKeyPath)
	if err != nil {
		log.Fatalln(err)
		return err
	}

	return cert.CreateCACertificate(
		&cert.CACertificateConfig{
			PrivKey:            privKey,
			HashType:           hashType,
			CertPath:           path,
			CertFileName:       "ca.crt",
			Country:            c,
			Locality:           l,
			Province:           p,
			OrganizationalUnit: ou,
			Organization:       org,
			CommonName:         cn,
			ExpireYear:         expireYear,
			Sans:               sans,
		},
	)
}

func DelayCert(path, caKeyPath, caCertPath, name string, expireYear int32, hashType crypto.HashType, pairType KeyPairType) error {

	keyName := fmt.Sprintf("%s.key", name)
	csrName := fmt.Sprintf("%s.csr", name)
	certName := fmt.Sprintf("%s.crt", name)
	csrPath := filepath.Join(path, csrName)
	defer os.Remove(csrPath)

	keyPath := filepath.Join(path, keyName)

	if _, err := os.Stat(keyPath); os.IsNotExist(err) {
		log.Fatalln(err)
		return err
	}
	certPath := filepath.Join(path, certName)
	if _, err := os.Stat(certPath); os.IsNotExist(err) {
		log.Fatalln(err)
		return err
	}
	oldcert, err := parseCertificate(certPath)
	if err != nil {
		log.Fatalln(err)
		return err
	}
	data := time.Now()
	back := filepath.Join(path, fmt.Sprintf("%s.%d", certName, data.Unix()))
	os.Rename(certPath, back)

	var keyUsages []x509.KeyUsage

	switch pairType {
	case KeyPairTypeUserSign, KeyPairTypeNodeSign:
		keyUsages = []x509.KeyUsage{x509.KeyUsageDigitalSignature, x509.KeyUsageContentCommitment}
	case KeyPairTypeUserTLS:
		keyUsages = []x509.KeyUsage{
			x509.KeyUsageKeyEncipherment,
			x509.KeyUsageDataEncipherment,
			x509.KeyUsageKeyAgreement,
			x509.KeyUsageDigitalSignature,
			x509.KeyUsageContentCommitment,
		}
	case KeyPairTypeUserTLSEnc:
		keyUsages = []x509.KeyUsage{
			x509.KeyUsageKeyEncipherment,
			x509.KeyUsageDataEncipherment,
			x509.KeyUsageKeyAgreement,
		}
	case KeyPairTypeNodeTLS:
		keyUsages = []x509.KeyUsage{
			x509.KeyUsageKeyEncipherment,
			x509.KeyUsageDataEncipherment,
			x509.KeyUsageKeyAgreement,
			x509.KeyUsageDigitalSignature,
			x509.KeyUsageContentCommitment,
		}
	case KeyPairTypeNodeTLSEnc:
		keyUsages = []x509.KeyUsage{
			x509.KeyUsageKeyEncipherment,
			x509.KeyUsageDataEncipherment,
			x509.KeyUsageKeyAgreement,
		}
	}

	privKey, err := parsePrivateKey(keyPath)
	if err != nil {
		log.Fatal(err)
		return err
	}

	if err := cert.CreateCSR(
		&cert.CSRConfig{
			PrivKey:            privKey,
			CsrPath:            path,
			CsrFileName:        csrName,
			Country:            oldcert.Subject.Country[0],            // c,
			Locality:           oldcert.Subject.Locality[0],           // l,
			Province:           oldcert.Subject.Province[0],           // p,
			OrganizationalUnit: oldcert.Subject.OrganizationalUnit[0], // ou,
			Organization:       oldcert.Subject.Organization[0],       // org,
			CommonName:         oldcert.Subject.CommonName,            // cn,
		},
	); err != nil {
		log.Fatal(err)
		return err
	}

	sans := []string{}
	sans = append(sans, oldcert.DNSNames...)
	for _, ip := range oldcert.IPAddresses {
		sans = append(sans, ip.String())
	}
	if err := cert.IssueCertificate(
		&cert.IssueCertificateConfig{
			HashType:              hashType,
			IssuerPrivKeyFilePath: caKeyPath,
			IssuerCertFilePath:    caCertPath,
			CsrFilePath:           csrPath,
			CertPath:              path,
			CertFileName:          certName,
			ExpireYear:            expireYear,
			Sans:                  sans, // oldcert.DNSNames, oldcert.IPAddresses
			KeyUsages:             keyUsages,
			ExtKeyUsages:          oldcert.ExtKeyUsage,
		},
	); err != nil {
		log.Fatal(err)
		return err
	}

	return nil
}
