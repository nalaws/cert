package cmcrypto

import (
	"crypto/x509"
	"fmt"
	"io/ioutil"

	"chainmaker.org/chainmaker/common/v2/cert"
	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/common/v2/crypto/asym"
	"chainmaker.org/chainmaker/tools/cert/config"
)

func parsePrivateKey(path string) (crypto.PrivateKey, error) {
	privKeyRaw, err := ioutil.ReadFile(path)
	if err != nil {
		err = fmt.Errorf("read private key file [%s] failed, %s", path, err)
		return nil, err
	}

	if cert.P11Context != nil /*&& cert.P11Context.enable */ {
		privKey, err := cert.ParseP11PrivKey( /*cert.P11Context.handle*/ config.PKCS11Handle, privKeyRaw)
		if err != nil {
			err = fmt.Errorf("parse pkcs11 privakey failed, %s", err)
			return nil, err
		}
		return privKey, nil
	} else {
		privKey, err := asym.PrivateKeyFromPEM(privKeyRaw, []byte{})
		if err != nil {
			err = fmt.Errorf("PrivateKeyFromPEM failed, %s", err)
			return nil, err
		}
		return privKey, nil
	}
}

func parseCertificate(path string) (*x509.Certificate, error) {
	return cert.ParseCertificate(path)
}
