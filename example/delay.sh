#!/usr/bin/env bash
#
# Copyright (C) BABEC. All rights reserved.
# Copyright (C) May A29 Limited, XZP. All rights reserved.
#
# SPDX-License-Identifier: Apache-2.0
#

MODE=$1

function show_help() {
    echo "Usage:  "
    echo "  delay.sh <type>"
    echo "    eg1: delay.sh ca"
    echo "    eg2: delay.sh node"
    echo "    eg2: delay.sh user"
}

if [[ $MODE == "ca" ]]; then
    ../cert ca -c crypto_config.yml -p pkcs11_keys.yml --ca ca
elif [[ $MODE == "node" ]]; then
    ../cert node -c crypto_config.yml -p pkcs11_keys.yml --ca ca --cert node/consensus1/consensus1.sign.crt -y 2
elif [[ $MODE == "user" ]]; then
    ../cert user -c crypto_config.yml -p pkcs11_keys.yml --ca ca --cert user/client1/client1.sign.crt -y 1
else
    show_help
    exit 1
fi