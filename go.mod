module chainmaker.org/chainmaker/tools/cert

go 1.16

require (
	chainmaker.org/chainmaker/common/v2 v2.3.0
	github.com/pkg/errors v0.9.1
	github.com/spf13/cobra v1.1.1
	github.com/spf13/viper v1.15.0
)

replace github.com/spf13/viper => github.com/spf13/viper v1.7.1 //for go1.15 build
