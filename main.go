package main

import (
	"log"

	"github.com/spf13/cobra"

	"chainmaker.org/chainmaker/tools/cert/command"
	"chainmaker.org/chainmaker/tools/cert/config"
)

func main() {
	mainCmd := &cobra.Command{
		Use: "cert",
		PersistentPreRun: func(cmd *cobra.Command, args []string) {
			config.LoadCryptoGenConfig(command.ConfigPath)
			if config.PKCS11Enabled() {
				if err := config.LoadPKCS11KeysConfig(command.P11KeysPath); err != nil {
					log.Fatalln(err)
					return
				}
			}
		},
	}
	mainFlags := mainCmd.PersistentFlags()
	mainFlags.StringVarP(&command.ConfigPath, "config", "c", "./crypto_config.yml", "specify config file path")
	mainFlags.StringVarP(&command.P11KeysPath, "pkcs11_keys", "p", "./pkcs11_keys.yml", "specify pkcs11 keys file path")
	mainFlags.StringVarP(&command.OutputDir, "output", "o", "crypto-config", "specify the output directory in which to place artifacts")

	mainCmd.AddCommand(command.ShowConfigCmd())
	mainCmd.AddCommand(command.CACmd())
	mainCmd.AddCommand(command.NodeCmd())
	mainCmd.AddCommand(command.UserCmd())

	if err := mainCmd.Execute(); err != nil {
		log.Fatalf("failed to execute, err = %s", err)
	}
}
